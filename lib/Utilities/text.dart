import 'package:flutter/material.dart';

setFuturaHF(String text, Color color, double size) {
  return Text(text,
      style: TextStyle(
          color: color, fontSize: size, fontFamily: "Futura Heavy font"));
}
