// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:rewardz/Screens/new.dart';
import 'package:rewardz/Utilities/colors.dart';
import 'package:rewardz/Utilities/constants.dart';
import 'package:rewardz/Utilities/container.dart';
import 'package:rewardz/Utilities/global.dart';
import 'package:rewardz/Utilities/text.dart';

class logInScreen extends StatefulWidget {
  @override
  _logInScreen createState() => _logInScreen();
}

class _logInScreen extends State<logInScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: ScreenUtilInit(builder: (BuildContext context, child) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Column(children: [
              setAppbarContainer(
                Padding(
                  padding: EdgeInsets.only(bottom: 11.0.h),
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: setFuturaHF(rewardz, white, 26)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Image.asset("assets/images/applogo.png",
                    width: 168.w, height: 155.h),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.0.h),
                child: setFuturaHF(login, orange, 17),
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.0.w, right: 15.w),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: setContainer(
                          Center(child: setFuturaHF(email, white, 17)),
                          23.h,
                          8.r),
                    ),
                    setContainerTextField(),
                    setDivider(2.h, double.infinity, orange2),
                    Padding(
                      padding: EdgeInsets.only(top: 17.h),
                      child: setContainer(
                          Center(child: setFuturaHF(pswd, white, 17)),
                          23.h,
                          8.r),
                    ),
                    setContainerTextField(),
                    setDivider(2.h, double.infinity, orange2),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          index = 0;
                          count = 1;
                        });
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => newOrder()),
                        );
                      },
                      child: Padding(
                          padding: EdgeInsets.only(top: 17.0.h),
                          child: setContainer(
                              Center(child: setFuturaHF(login, white, 23)),
                              76.h,
                              8.r)),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        );
      }),
    );
  }
}
