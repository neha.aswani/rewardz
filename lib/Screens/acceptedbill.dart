import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rewardz/Screens/accepted.dart';
import 'package:rewardz/Screens/delivery.dart';
import 'package:rewardz/Utilities/container.dart';
import 'package:rewardz/Utilities/global.dart';
import 'package:rewardz/Utilities/text.dart';
import 'package:rewardz/Widgets/tabbar.dart';

import '../Utilities/colors.dart';
import '../Utilities/constants.dart';

class acceptedbill extends StatefulWidget {
  @override
  _acceptedbill createState() => _acceptedbill();
}

class _acceptedbill extends State<acceptedbill> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
            child: Column(children: [
      tabbar(),
      Expanded(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Column(
                  children: [
                    setListViewContainer(
                        Padding(
                            padding: EdgeInsets.only(
                              left: 14.w,
                              right: 14.w,
                              top: 12.75.h,
                              bottom: 81.25.h,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: orange2, width: 1.h)),
                                          color: white),
                                      child: Row(children: [
                                        setFuturaHF(name, greyColor, 13),
                                        SizedBox(width: 5.w),
                                        setFuturaHF("-", greyColor, 13),
                                      ]),
                                    ),
                                    SizedBox(width: 5.w),
                                    setFuturaHF(nvalue, black, 13)
                                  ],
                                ),
                                SizedBox(
                                  height: 11.5.h,
                                ),
                                Row(
                                  children: [
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          setFuturaHF(to, greyColor, 13),
                                          setDivider(1.h, 94.w, orange2)
                                        ]),
                                    SizedBox(width: 5.w),
                                    setFuturaHF("-", greyColor, 13),
                                    SizedBox(width: 5.w),
                                    setFuturaHF(tovalue, black, 13)
                                  ],
                                ),
                                SizedBox(
                                  height: 11.5.h,
                                ),
                                Row(
                                  children: [
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          setFuturaHF(daddr, greyColor, 13),
                                          setDivider(1.h, 94.w, orange2)
                                        ]),
                                    SizedBox(width: 5.w),
                                    setFuturaHF("-", greyColor, 13),
                                    SizedBox(width: 5.w),
                                    setFuturaHF("$daddrv 1", black, 13)
                                  ],
                                ),
                                const SizedBox(height: 6.5),
                                setFuturaHF("$daddrv 2", black, 13),
                                const SizedBox(height: 6.5),
                                setFuturaHF("$daddrv 3", black, 13),
                                SizedBox(height: 16.5.h),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        setFuturaHF(order, greyColor, 13),
                                        setDivider(1.h, 94.w, orange2),
                                        const SizedBox(
                                          height: 11,
                                        ),
                                        setFuturaHF("1x $menui 1", black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF("1x $menui 2", black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF("2x $menui 3", black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF("1x $menui 4", black, 13),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        setFuturaHF(price, greyColor, 13),
                                        setDivider(1.h, 94.w, orange2),
                                        const SizedBox(
                                          height: 11,
                                        ),
                                        setFuturaHF(pricev, black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF(pricev, black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF(pricev, black, 13),
                                        const SizedBox(height: 10.5),
                                        setFuturaHF(pricev, black, 13),
                                        SizedBox(height: 17.h),
                                        setDivider(1.h, 94.w, orange2),
                                        SizedBox(height: 3.5.h),
                                        setFuturaHF(total, greyColor, 13),
                                        SizedBox(
                                          height: 3.5.h,
                                        ),
                                        setDivider(1.h, 94.w, orange2),
                                        SizedBox(
                                          height: 10.5.h,
                                        ),
                                        setFuturaHF(pricev2, black, 13)
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )),
                        8.r)
                  ],
                ),
              ),
              SizedBox(height: 15.h),
              GestureDetector(
                onTap: () {
                  index = 2;

                  Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, animation1, animation2) =>
                            deliveryOrder(),
                        transitionDuration: Duration.zero,
                        reverseTransitionDuration: Duration.zero,
                      ));
                },
                child: Padding(
                    padding:
                        EdgeInsets.only(left: 20.w, right: 20.w, bottom: 77.h),
                    child: setContainer(
                        Center(child: setFuturaHF(ofd, white, 23)), 39.h, 8.r)),
              ),
              GestureDetector(
                onTap: () {
                  index = 1;

                  Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, animation1, animation2) =>
                            acceptedOrder(),
                        transitionDuration: Duration.zero,
                        reverseTransitionDuration: Duration.zero,
                      ));
                },
                child: Padding(
                    padding:
                        EdgeInsets.only(left: 20.w, right: 20.w, bottom: 15.h),
                    child: setListViewContainerwithHeight(
                        Center(child: setFuturaHF(back, orange3, 23)),
                        39.h,
                        8.r)),
              )
            ],
          ),
        ),
      ),
    ])));
  }
}
